(ns plf01.core)

(defn función-<-1
  [a b]
  (< a b))

(defn función-<-2
  [a b c]
  (< a b c))

(defn función-<-3
  [a b c d]
  (< a b c d))

(función-<-1 30 20)
(función-<-2 10 30 30)
(función-<-3 4 8 38 119)

(defn función-<=-1
  [a b]
  (<= a b))

(defn función-<=-2
  [a b c]
  (<= a b c))

(defn función-<=-3
  [a b c d]
  (<= a b c d))

(función-<=-1 101 100)
(función-<=-2 10 27 27)
(función-<=-3 9 18 11 1)

(defn función-==-1
  [a b]
  (== a b))

(defn función-==-2
  [a b c]
  (== a b c))

(defn función-==-3
  [a b c d]
  (== a b c d))

(función-==-1 1 1.0)
(función-==-2 89 90 89)
(función-==-3 41 41 41 41)

(defn función->-1
  [a b]
  (> a b))

(defn función->-2
  [a b c]
  (> a b c))

(defn función->-3
  [a b c d]
  (> a b c d))

(función->-1 10 13)
(función->-2 98 65 31)
(función->-3 101 90 56 23)

(defn función->=-1
  [a b]
  (>= a b))

(defn función->=-2
  [a b c]
  (>= a b c))

(defn función->=-3
  [a b c d]
  (>= a b c d))

(función->=-1 66 66)
(función->=-2 90 11 67)
(función->=-3 99 96 69 60)

(defn función-assoc-1
  [a b c]
  (assoc a b c))

(defn función-assoc-2
  [a b c]
  (assoc a b c))

(defn función-assoc-3
  [a b c]
  (assoc a b c))

(función-assoc-1 {10 20 30 40} 10 100)
(función-assoc-2 [90 70 50 30 10] 2 500)
(función-assoc-3 {10 100 20 200 30 300} 40 400)

(defn función-assoc-in-1
  [a b c]
  (assoc-in a b c))

(defn función-assoc-in-2
  [a b c]
  (assoc-in a b c))

(defn función-assoc-in-3
  [a b c]
  (assoc-in a b c))

(función-assoc-in-1 [{:nombre "Fanny" :edad 23} {:nombre "Bere" :edad 20}] [1 :edad] 17)
(función-assoc-in-2 {:usuario {:edad 50}} [:usuario :id] 123)
(función-assoc-in-3 [[0 0 0] [0 0 0] [0 0 0]] [2 2] 1)

(defn función-concat-1
  [a b]
  (concat a b))

(defn función-concat-2
  [a b c]
  (concat a b c))

(defn función-concat-3
  [a b]
  (concat a b))

(función-concat-1 "Hola" "¿Cómo estás?")
(función-concat-2 '(90 80 70) {10 100 20 200} #{6 7 8})
(función-concat-3 #{1 2 3 4 5} "Hola")

(defn función-conj-1
  [a b]
  (conj a b))

(defn función-conj-2
  [a b]
  (conj a b))

(defn función-conj-3
  [a b c]
  (conj a b c))

(función-conj-1 {1 2 3 4} [5 6])
(función-conj-2 #{9 8 7 6} [1 2])
(función-conj-3 '("a" "b" "c") "Hola" 66)

(defn función-cons-1
  [a b]
  (cons a b))

(defn función-cons-2
  [a b]
  (cons a b))

(defn función-cons-3
  [a b]
  (cons a b))

(función-cons-1 123 '("a" "b" "c"))
(función-cons-2 [50 120] {9 90 7 70 4 40})
(función-cons-3 {:nombre "Berenice" :id 987} [100 10])

(defn función-contains?-1
  [a b]
  (contains? a b))

(defn función-contains?-2
  [a b]
  (contains? a b))

(defn función-contains?-3
  [a b]
  (contains? a b))

(función-contains?-1 {:nombre "Estefania" :edad 23 :id 12345} :edad)
(función-contains?-2 ["Hola" "Hello" "Ciao"] "Hello")
(función-contains?-3 "Hola ¿Cómo estás?" 20)

(defn función-count-1
  [a]
  (count a))

(defn función-count-2
  [a]
  (count a))

(defn función-count-3
  [a]
  (count a))

(función-count-1 "Hola ¿Cómo te llamas?")
(función-count-2 '("Hola" :etiqueta 1234 \A))
(función-count-3 [\E \s \t \e \f \a \n \i \a])

(defn función-disj-1
  [a b]
  (disj a b))

(defn función-disj-2
  [a b c]
  (disj a b c))

(defn función-disj-3
  [a b]
  (disj a b))

(función-disj-1 #{"hola" "adiós" "hasta luego" "hasta pronto"} "hola")
(función-disj-2 #{:a :b :c :d :e :f :g} :z :d)
(función-disj-3 #{[1 2 3] [4 5 6] [7 8] [9] [0]} [7 8])

(defn función-dissoc-1
  [a b]
  (dissoc a b))

(defn función-dissoc-2
  [a b]
  (dissoc a b))

(defn función-dissoc-3
  [a b c]
  (dissoc a b c))

(función-dissoc-1 {:w 80 :x 100 :y 120 :z 140} :z)
(función-dissoc-2 {:a \A :b \B :c \C} :c)
(función-dissoc-3 {0 '(1 2 3) 1 '(0 9 8) 2 '(0 0 0) 3 '(5 6) 4 "hola" 5 "adios"} 4 5)

(defn función-distinct-1
  [a]
  (distinct a))

(defn función-distinct-2
  [a]
  (distinct a))

(defn función-distinct-3
  [a]
  (distinct a))

(función-distinct-1 '(1 2 3 4 5 7 6 4 3 1 7 9 9 6 4 3 6 7))
(función-distinct-2 [0 0 0 0 9 8 6 5 6 8 9 0 8 7 6 6 7 9 0])
(función-distinct-3 "abcdabcdabcd")

(defn función-distinct?-1
  [a b c d e f]
  (distinct? a b c d e f))

(defn función-distinct?-2
  [a b c]
  (distinct? a b c))

(defn función-distinct?-3
  [a b c d e]
  (distinct? a b c d e))

(función-distinct?-1 6 6 9 8 4 1)
(función-distinct?-2 '("a" "b" "c") '("g" "h" "i") '("d" "e" "f"))
(función-distinct?-3 "hola" "adios" "hello" "Adios" "ADIOS")

(defn función-drop-last-1
  [a]
  (drop-last a))

(defn función-drop-last-2
  [a b]
  (drop-last b a))

(defn función-drop-last-3
  [a b]
  (drop-last b a))

(función-drop-last-1 {:a 1 :b 2 :c 3 :d 4 :e 5})
(función-drop-last-2 '(#{10 20 30 40} "hola" [1 2 3] #{9 8 7 6} "adios" 987) 4)
(función-drop-last-3 [[1 2 3] [4 5 6] [7 8] [9] [0]] 2)

(defn función-empty-1
  [a]
  (empty a))

(defn función-empty-2
  [a]
  (empty a))

(defn función-empty-3
  [a]
  (empty a))

(función-empty-1 [#{10 20 30 40} "hola" [1 2 3] #{9 8 7 6} "adios" 987])
(función-empty-2 '([1 2 3] [4 5 6] [7 8] [9] [0]))
(función-empty-3 "Hola, ¿cómo te llamas?")

(defn función-empty?-1
  [a]
  (empty? a))

(defn función-empty?-2
  [a]
  (empty? a))

(defn función-empty?-3
  [a]
  (empty? a))

(función-empty?-1 ["" [] () '() {} #{} nil])
(función-empty?-2 '("hola" #{} [:a :b :c] {:h "hi" :b "bye"} '()))
(función-empty?-3 "")

(defn función-even?-1
  [a]
  (even? a))

(defn función-even?-2
  [a]
  (even? a))

(defn función-even?-3
  [a]
  (even? a))

(función-even?-1 3)
(función-even?-2 50)
(función-even?-3 20)

(defn función-false?-1
  [a]
  (false? a))

(defn función-false?-2
  [a]
  (false? a))

(defn función-false?-3
  [a]
  (false? a))

(función-false?-1 false)
(función-false?-2 10)
(función-false?-3 [false true true false])

(defn función-find-1
  [a b]
  (find a b))

(defn función-find-2
  [a b]
  (find a b))

(defn función-find-3
  [a b]
  (find a b))

(función-find-1 {:a \A :b \B :c \C :d \D} :e)
(función-find-2 [1 2 3 4 5 6] 2)
(función-find-3 {1 "ab" 2 "cd" 3 "ef"} 3)

(defn función-first-1
  [a]
  (first a))

(defn función-first-2
  [a]
  (first a))

(defn función-first-3
  [a]
  (first a))

(función-first-1 [:abc [1 2 3] "hola" \X])
(función-first-2 '(123 {1 :a} "adios" '("lista")))
(función-first-3 #{'("lista") [1 2 3 4 5] {:a "a" :b "b"} #{1 6 9 0}})

(defn función-flatten-1
  [a]
  (flatten a))

(defn función-flatten-2
  [a]
  (flatten a))

(defn función-flatten-3
  [a]
  (flatten a))

(función-flatten-1 '("hola" "adios" [1 2 3 4 5]))
(función-flatten-2 [:abc [1 2 3] "hola" \X [4 5 6]])
(función-flatten-3 {'("lista") [1 2 3 4 5] {:a "a" :b "b"} #{1 6 9 0}})

(defn función-frequencies-1
  [a]
  (frequencies a))

(defn función-frequencies-2
  [a]
  (frequencies a))

(defn función-frequencies-3
  [a]
  (frequencies a))

(función-frequencies-1 [1 1 5 7 7 9 3 4 5 6 7 3])
(función-frequencies-2 '(100 110 300 300 200 110 100))
(función-frequencies-3 "Estefania")

(defn función-get-1
  [a b]
  (get a b))

(defn función-get-2
  [a b]
  (get a b))

(defn función-get-3
  [a b]
  (get a b))

(función-get-1 {1 100 2 200 3 300 :a 1 :b 2 :c 3} :a)
(función-get-2 [100 200 300 400 500 600] 2)
(función-get-3 "Estefania Berenice" 10)

(defn función-get-in-1
  [x y]
  (get-in x y))

(defn función-get-in-2
  [a b]
  (get-in a b))

(defn función-get-in-3
  [x y]
  (get-in x y))

(función-get-in-1 [[1 2 3] [4 5 6 7 8] [9 0]] [1 3])
(función-get-in-2 {:mascotas [{:nombre "Fairy" :tipo "perro"} {:nombre "Colitas" :tipo "gato"}]} [:mascotas 0 :nombre])
(función-get-in-3 [{1 :Estefania} {2 :Berenice 2.1 :Fanny} {3 :Ortega} {4 :Martínez}] [1 2])

(defn función-into-1
  [a b]
  (into b a))

(defn función-into-2
  [a b]
  (into a b))

(defn función-into-3
  [a b]
  (into b a))

(función-into-1 [[1 10] [2 20] [3 30] [4 40] [5 50]] #{})
(función-into-2 {:a 100 :b 200 :c 300} '())
(función-into-3 #{90 80 70 60 50} [])

(defn función-key-1
  [a]
  (key a))

(defn función-key-2
  [a]
  (key a))

(defn función-key-3
  [a]
  (key a))

(función-key-1 (first {:h "hola" :j "juguete" :k "koala"}))
(función-key-2 (second {:nombre "Fairy" :tipo "perro" :nombremascota "Colitas" :tipomascota "gato"}))
(función-key-3 (last {#{"uno"} 1 #{"dos"} 2 #{"tres"} 3}))

(defn función-keys-1
  [m]
  (keys m))

(defn función-keys-2
  [m]
  (keys m))

(defn función-keys-3
  [m]
  (keys m))

(función-keys-1 {11 :a 22 :b 33 :c 44 :d})
(función-keys-2 {:mascotas [{:nombre "Fairy" :tipo "perro"} {:nombre "Colitas" :tipo "gato"}]
                 :dueños [{:nombre "Estefania"} {:nombre "Gabriela"}]})
(función-keys-3 {#{"uno"} 1 #{"dos"} 2 #{"tres"} 3})

(defn función-max-1
  [a b c d e f g]
  (max a b c d e f g))

(defn función-max-2
  [a b]
  (max a b))

(defn función-max-3
  [a b c d e f]
  (max a b c d e f))

(función-max-1 90 65 37 80 100 1478 90238)
(función-max-2 100 101)
(función-max-3 78 3435 987674 21452 98172 4724)

(defn función-merge-1
  [a b]
  (merge a b))

(defn función-merge-2
  [a b c]
  (merge a b c))

(defn función-merge-3
  [a b]
  (merge a b))

(función-merge-1 {:a 100 :b 200 :c 300} {:a 100 :d 400})
(función-merge-2 {:h "hola" :a "adios" :b "bye"} {:x [10 11 12] :y [13 14 15]} {:z #{1 2 3 4 5}})
(función-merge-3 {:z #{1 2 3 4 5}} {:x #{90 80 70 60} :y #{5 10 15 20 25}})

(defn función-min-1
  [a b c d e f g]
  (min a b c d e f g))

(defn función-min-2
  [a b]
  (min a b))

(defn función-min-3
  [a b c]
  (min a b c))

(función-min-1 90 65 37 80 100 1478 90238)
(función-min-2 100 101)
(función-min-3 987674 21452 98172)

(defn función-neg?-1
  [n]
  (neg? n))

(defn función-neg?-2
  [n]
  (neg? n))

(defn función-neg?-3
  [n]
  (neg? n))

(función-neg?-1 100)
(función-neg?-2 -90238)
(función-neg?-3 -8/2)

(defn función-nil?-1
  [a]
  (nil? a))

(defn función-nil?-2
  [a]
  (nil? a))

(defn función-nil?-3
  [x]
  (nil? x))

(función-nil?-1 "Estefania")
(función-nil?-2 [])
(función-nil?-3 nil)

(defn función-not-empty-1
  [a]
  (not-empty a))

(defn función-not-empty-2
  [a]
  (not-empty a))

(defn función-not-empty-3
  [a]
  (not-empty a))

(función-not-empty-1 "")
(función-not-empty-2 '(4 5 7 8 6 5 3))
(función-not-empty-3 ['() #{} {} []])

(defn función-nth-1
  [a b]
  (nth a b))

(defn función-nth-2
  [a b]
  (nth a b))

(defn función-nth-3
  [a b]
  (nth a b))

(función-nth-1 ['() #{} {} []] 1)
(función-nth-2 '(:a 90 :b 100 :c 200 :d 310) 2)
(función-nth-3 "ESTEFANIA ORTEGA" 4)

(defn función-odd?-1
  [a]
  (odd? a))

(defn función-odd?-2
  [a]
  (odd? a))

(defn función-odd?-3
  [a]
  (odd? a))

(función-odd?-1 5)
(función-odd?-2 -60)
(función-odd?-3 0)

(defn función-partition-1
  [a b]
  (partition b a))

(defn función-partition-2
  [a b]
  (partition b a))

(defn función-partition-3
  [a b]
  (partition b a))

(función-partition-1 [1 2 3 4 5 6 7 8 9] 3)
(función-partition-2 '(:a :b :c :d :e :f :g :h :i :j :k :l) 5)
(función-partition-3 "abcdefghijklmnñopqrstuvwxyz" 2)

(defn función-partition-all-1
  [a b]
  (partition-all b a))

(defn función-partition-all-2
  [a b]
  (partition-all b a))

(defn función-partition-all-3
  [a b]
  (partition-all b a))

(función-partition-all-1 [1 2 3 4 5 6 7 8 9] 4)
(función-partition-all-2 '(:a :b :c :d :e :f :g :h :i :j :k :l :m) 3)
(función-partition-all-3 (range 27) 2)

(defn función-peek-1
  [a]
  (peek a))

(defn función-peek-2
  [a]
  (peek a))

(defn función-peek-3
  [a]
  (peek a))

(función-peek-1 [1 2 3 4 5 6 7 8 9 5 8 9 6 4 2 7 9 8 5 8 5 32 5 7])
(función-peek-2 '(:a :b :c :d :e :f :g :h :i :j :k :l :m))
(función-peek-3 ['() [] [] '()])

(defn función-pop-1
  [a]
  (pop a))

(defn función-pop-2
  [a]
  (pop a))

(defn función-pop-3
  [a]
  (pop a))

(función-pop-1 [1 2 3 4 5 6 7 8 9 5 8 9 6 4 2 7 9 8 5 8 5 32 5 7])
(función-pop-2 '(:a :b :c :d :e :f :g :h :i :j :k :l :m))
(función-pop-3 ['(1 2 3) [4 5 6] [7 8 9] '(0)])

(defn función-pos?-1
  [n]
  (pos? n))

(defn función-pos?-2
  [n]
  (pos? n))

(defn función-pos?-3
  [n]
  (pos? n))

(función-pos?-1 10)
(función-pos?-2 -1/2)
(función-pos?-3 -9.0)

(defn función-quot-1
  [a b]
  (quot a b))

(defn función-quot-2
  [a b]
  (quot a b))

(defn función-quot-3
  [a b]
  (quot a b))

(función-quot-1 9 3)
(función-quot-2 -5.9 2)
(función-quot-3 18/3 3)

(defn función-range-1
  [a b]
  (range a b))

(defn función-range-2
  [b]
  (range b))

(defn función-range-3
  [a b c]
  (range a b c))

(función-range-1 10 30)
(función-range-2 16)
(función-range-3 70 100 5)

(defn función-rem-1
  [n d]
  (rem n d))

(defn función-rem-2
  [n d]
  (rem n d))

(defn función-rem-3
  [n d]
  (rem n d))

(función-rem-1 9 2)
(función-rem-2 -5.9 2)
(función-rem-3 18/3 5)

(defn función-repeat-1
  [a]
  (repeat a))

(defn función-repeat-2
  [a b]
  (repeat b a))

(defn función-repeat-3
  [a b]
  (repeat b a))

(función-repeat-1 "hi")
(función-repeat-2 "bye" 10)
(función-repeat-3 [1 2 3] 6)

(defn función-replace-1
  [a b]
  (replace a b))

(defn función-replace-2
  [a b]
  (replace a b))

(defn función-replace-3
  [a b]
  (replace a b))

(función-replace-1 [10 9 8 7 6] [0 2 4])
(función-replace-2 {2 :dos, 4 :cuatro} [4 2 3 4 5 6 2])
(función-replace-3 [:cero :uno :dos :tres :cuatro] [0 2 4 0])

(defn función-rest-1
  [a]
  (rest a))

(defn función-rest-2
  [a]
  (rest a))

(defn función-rest-3
  [a]
  (rest a))

(función-rest-1 [[7 6 4 3] {5 50 7 70 9 90} '(7 5 3 3)])
(función-rest-2 '(8 6 4 3 5 7 9 0 9))
(función-rest-3 #{:a :b :c :d :e :f :g})

(defn función-select-keys-1
  [a b]
  (select-keys a b))

(defn función-select-keys-2
  [a b]
  (select-keys a b))

(defn función-select-keys-3
  [a b]
  (select-keys a b))

(función-select-keys-1 {:h "hola", :a "adios", :b "bye", :x [10 11 12], :y [13 14 15], :z #{1 4 3 2 5}} [:h :b :y])
(función-select-keys-2 [[7 6 4 3] {5 50 7 70 9 90} '(7 5 3 3) {:a 10 :b 20 :c 30}] [3 0])
(función-select-keys-3 {:a '(8 6 4 3) :b #{5 7 0 9} :c [9 6 0]} [:b :a :c])

(defn función-shuffle-1
  [a]
  (shuffle a))

(defn función-shuffle-2
  [a]
  (shuffle a))

(defn función-shuffle-3
  [a]
  (shuffle a))

(función-shuffle-1 '(8 6 4 3 5 7 9 0 9))
(función-shuffle-2 #{90 85 80})
(función-shuffle-3 [7 6 4 3 5 50 7 70 9 90 7 5 3 3 :a 10 :b 20 :c 30])

(defn función-sort-1
  [a]
  (sort a))

(defn función-sort-2
  [a]
  (sort a))

(defn función-sort-3
  [a]
  (sort a))

(función-sort-1 '(8 6 4 3 5 7 9 0 9))
(función-sort-2 '([8 6 3 2] [5 43 7 9] [1 3 4 6 7]))
(función-sort-3 [7 6 4 3 5 50 7 70 9 90 7 5 3 3 10 20 30])

(defn función-split-at-1
  [a x]
  (split-at x a))

(defn función-split-at-2
  [a x]
  (split-at x a))

(defn función-split-at-3
  [a x]
  (split-at x a))

(función-split-at-1 '(8 6 4 3 5 7 9 0 9) 3)
(función-split-at-2 '([8 6 3 2] [5 43 7 9] [1 3 4 6 7]) 2)
(función-split-at-3 [7 6 4 3 5 50 7 70 9 90 7 5 3 3 10 20 30] 6)

(defn función-str-1
  [a b c d]
  (str a b c d))

(defn función-str-2
  [a b]
  (str a b))

(defn función-str-3
  [a b c]
  (str a b c))

(función-str-1 987 432 678 0000)
(función-str-2 "Hola ¿cómo te llamas?" "Hasta luego")
(función-str-3 [7 6 4] [:a 5 50 :b 70  90] [:c 5 \W 3 10 :d \Z])

(defn función-subs-1
  [a b]
  (subs a b))

(defn función-subs-2
  [a b c]
  (subs a b c))

(defn función-subs-3
  [a b c]
  (subs a b c))

(función-subs-1 "Mi perrita se llama Fayri" 6)
(función-subs-2 "Mi color favorito es el morado" 18 20)
(función-subs-3 "9 7 4 2 1 4 6 8 -1/4 1/9 7/3 -9/2" 15 21)

(defn función-subvec-1
  [a b] 
  (subvec a b))

(defn función-subvec-2
  [a b c]
  (subvec a b c))

(defn función-subvec-3
  [a b]
  (subvec a b))

(función-subvec-1 [9 7 6 3 1 34 5 7 9 1.7788 8.421 -1/30] 9)
(función-subvec-2 [9 7 4 2 1 4 6 8 -1/4 1/9 7/3 -9/2 754.345 9/20 -75] 11 13)
(función-subvec-3 [[7 6 4 3] {5 50 7 70 9 90} '(7 5 3 3) {:a 10 :b 20 :c 30}] 2)

(defn función-take-1
  [a b]
  (take b a))

(defn función-take-2
  [a b]
  (take b a))

(defn función-take-3
  [a b]
  (take b a))

(función-take-1 [9 7 4 2 1 4 6 8 -1/4 1/9 7/3 -9/2 754.345 9/20 -75] 8)
(función-take-2 '(8 6 4 3 5 7 9 0 9) 3)
(función-take-3 "Mi color favorito no es el amarillo" 7)

(defn función-true?-1
  [a]
  (true? a))

(defn función-true?-2
  [a]
  (true? a))

(defn función-true?-3
  [a]
  (true? a))

(función-true?-1 true)
(función-true?-2 true)
(función-true?-3 false)

(defn función-val-1
  [a]
  (val a))

(defn función-val-2
  [a]
  (val a))

(defn función-val-3
  [a]
  (val a))

(función-val-1 (second {:h "hola" :a "adios" :b "bye" :x [10 11 12] :y [13 14 15] :z #{1 4 3 2 5}}))
(función-val-2 (first {:uno {:a 100 :b 200 :c 300} :dos {:a 100 :d 400}}))
(función-val-3 (last {\A :a \B :b \C :c \D :d \E :e}))

(defn función-vals-1
  [a]
  (vals a))

(defn función-vals-2
  [a]
  (vals a))

(defn función-vals-3
  [a]
  (vals a))

(función-vals-1 {:h "hola" :a "adios" :b "bye" :x [10 11 12] :y [13 14 15] :z #{1 4 3 2 5}})
(función-vals-2 {:uno {:a 100 :b 200 :c 300} :dos {:a 100 :d 400}})
(función-vals-3 {\A :a \B :b \C :c \D :d \E :e})

(defn función-zero?-1
  [a]
  (zero? a))

(defn función-zero?-2
  [a]
  (zero? a))

(defn función-zero?-3
  [a]
  (zero? a))

(función-zero?-1 0.001)
(función-zero?-2 0.0)
(función-zero?-3 7)

(defn función-zipmap-1
  [a b]
  (zipmap a b))

(defn función-zipmap-2
  [a b]
  (zipmap a b))

(defn función-zipmap-3
  [a b]
  (zipmap a b))

(función-zipmap-1 [1 2 3 4 5] [111 222 333 444 555])
(función-zipmap-2 '("a" "b" "c") '(1 2 3))
(función-zipmap-3 #{\A \B \C \D \E} #{:a :b :c :d :e})